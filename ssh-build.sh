#!/usr/bin/env bash
set -eo pipefail

docker build --quiet -t ssh-agent-forward $(dirname "$0")