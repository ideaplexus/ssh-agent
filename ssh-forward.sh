#!/usr/bin/env bash
set -eo pipefail

AUTHORIZED_KEYS=$(ssh-add -L | base64 | tr -d '\n')
KNOWN_HOSTS_FILE=$(mktemp -t dsaf.XXX)

HOST_PORT="2244"
IMAGE_NAME="ssh-agent-forward"
CONTAINER_NAME="ssh-agent-forward"
VOLUME_NAME="ssh-agent"

while test $# -gt 0
do
    case "$1" in
        --show-forwarded-keys)
            SHOW_FORWARDED_KEYS="1"
            ;;
        --build-image)
            BUILD_IMAGE="1"
            ;;
        --host-port)
            shift
            HOST_PORT=$1
            ;;
        --image-name)
            shift
            IMAGE_NAME=$1
            ;;
        --container-name)
            shift
            CONTAINER_NAME=$1
            ;;
        --volume-name)
            shift
            VOLUME_NAME=$1
            ;;
        --*) echo "bad option $1"
            ;;
    esac
    shift
done

if [ "$BUILD_IMAGE" ]; then
  eval $(dirname "$0")/ssh-build.sh >/dev/null
fi

trap 'rm ${KNOWN_HOSTS_FILE}' EXIT

docker rm -f "${CONTAINER_NAME}" >/dev/null 2>&1 || true

docker volume create --name "${VOLUME_NAME}" >/dev/null

docker run \
  --name "${CONTAINER_NAME}" \
  -e AUTHORIZED_KEYS="${AUTHORIZED_KEYS}" \
  -v ${VOLUME_NAME}:/ssh-agent \
  -d \
  -p "${HOST_PORT}:22" \
  "${IMAGE_NAME}" >/dev/null

if [ "${DOCKER_HOST}" ]; then
  HOST_IP=$(echo "$DOCKER_HOST" | awk -F '//' '{print $2}' | awk -F ':' '{print $1}')
else
  HOST_IP=127.0.0.1
fi

# FIXME Find a way to get rid of this additional 1s wait
sleep 1
while [ 1 ] && ! nc -z -w5 ${HOST_IP} ${HOST_PORT}; do sleep 0.1; done

ssh-keyscan -p "${HOST_PORT}" "${HOST_IP}" >"${KNOWN_HOSTS_FILE}" 2>/dev/null

if [ "$SHOW_FORWARDED_KEYS" ]; then
    ssh \
      -A \
      -o "UserKnownHostsFile=${KNOWN_HOSTS_FILE}" \
      -p "${HOST_PORT}" \
      -S none \
      "root@${HOST_IP}" \
      ssh-add -l
fi

# keep the agent running
ssh \
  -A \
  -f \
  -o "UserKnownHostsFile=${KNOWN_HOSTS_FILE}" \
  -p "${HOST_PORT}" \
  -S none \
  "root@${HOST_IP}" \
  /ssh-entrypoint.sh